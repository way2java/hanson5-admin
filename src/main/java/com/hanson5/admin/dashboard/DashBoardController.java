package com.hanson5.admin.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.hanson5.admin.greeting.service.GreetingService;

@Controller
public class DashBoardController {

  @Autowired
  GreetingService greetingService;

  @GetMapping("/")
  public String greeting(
      @RequestParam(name = "name", required = false, defaultValue = "World") String name,
      Model model) {

    greetingService.getGrettingMsg();

    model.addAttribute("name", name);
    return "dashboard/dashboard";
  }

}
