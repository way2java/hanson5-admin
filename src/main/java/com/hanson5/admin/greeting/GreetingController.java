package com.hanson5.admin.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.hanson5.admin.greeting.service.GreetingService;

@Controller
public class GreetingController {

  @Autowired
  GreetingService greetingService;

  @GetMapping("greeting")
  public String greeting(
      @RequestParam(name = "name", required = false, defaultValue = "World") String name,
      Model model) {

    greetingService.getGrettingMsg();

    model.addAttribute("name", name);
    return "greeting/greeting";
  }

}
