package com.hanson5.admin.greeting.model;

import lombok.Data;

@Data
public class Greeting {
  String id;

  String name;
}
