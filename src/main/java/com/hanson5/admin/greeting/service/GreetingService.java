package com.hanson5.admin.greeting.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hanson5.admin.common.Hanson5AdminService;
import com.hanson5.admin.greeting.mapper.GreetingMapper;

@Service
@Transactional
public class GreetingService implements Hanson5AdminService {

  @Autowired
  GreetingMapper greetingMapper;

  public void getGrettingMsg() {

    greetingMapper.getGrettingMsg();
  }
}
